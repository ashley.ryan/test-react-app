/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.NotFoundPage';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: '404',
  },
  metaDescription: {
    id: `${scope}.metaDescription`,
    defaultMessage: 'Page not found',
  },
  body: {
    id: `${scope}.body`,
    defaultMessage: 'Page not found',
  },
});

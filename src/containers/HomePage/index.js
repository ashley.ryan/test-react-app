import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import messages from './messages';
import { FormattedMessage } from 'react-intl';

function HomePage(props) {
  const {  } = props;

  return (
    <div className="" >
      <FormattedMessage {...messages.header} />
    </div>
  );
}

HomePage.propTypes = {
  //history: PropTypes.object.isRequired,
};

//export default compose()(HomePage);
export default HomePage;

/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage, injectIntl } from 'react-intl';
import messages from './messages';

function NotFound({ intl }) {
  return (
    <React.Fragment>
      <Helmet>
        <title>{intl.formatMessage(messages.title)}</title>
        <meta
          name="description"
          content={intl.formatMessage(messages.metaDescription)}
        />
      </Helmet>
      <h1>
        <FormattedMessage {...messages.body} />
      </h1>
    </React.Fragment>
  );
}

export default injectIntl(NotFound);

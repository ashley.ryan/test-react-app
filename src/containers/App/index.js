import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { Helmet } from 'react-helmet';

import GlobalStyle from '../../global-styles';
import { connect } from 'react-redux';
import { compose } from 'redux';
import messages from './messages';
import { injectIntl } from 'react-intl';

const { PUBLIC_URL } = process.env;

class App extends React.Component {
  /*componentDidMount() {
    const { } = this.props;
  }*/

  render() {
    const { intl } = this.props;

    return (
      <div className="" >
        <Helmet
          titleTemplate="%s - App"
          defaultTitle={intl.formatMessage(messages.title)}
        >
          <meta
            name="description"
            content={intl.formatMessage(messages.title)}
          />
        </Helmet>
        <Switch>
          <Route exact path={`${PUBLIC_URL}/`} component={HomePage} />
          {/*<Route path="/other" component={OtherPage} />*/}
          <Route component={NotFoundPage} />
        </Switch>
        <GlobalStyle />
      </div>
    );
  }
}

App.propTypes = {
  locale: PropTypes.string,
};

const mapDispatchToProps = (dispatch, ownProps) => ({

});

const withConnect = connect(
  undefined,
  mapDispatchToProps,
);

export default compose(injectIntl, withConnect)(App);

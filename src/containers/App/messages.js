/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AppPage';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Vistaprint graphic design',
  },
  metaDescription: {
    id: `${scope}.metaDescription`,
    defaultMessage: 'Vistaprint graphic design',
  },
});
